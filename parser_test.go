package main

import (
	"fmt"
	"testing"

	"github.com/rs/zerolog/log"
)

func Test_getType(t *testing.T) {

	want := "map[apiVersion:v1 kind:Pod metadata:map[labels:map[role:myrole] name:two] spec:map[containers:[map[image:nginx name:web ports:[map[containerPort:80 name:web protocol:TCP]]]]]]"

	var testYml = map[interface{}]interface{}{
		"apiVersion": "v1",
		"kind":       "Pod",
		"metadata": map[interface{}]interface{}{
			"name": "two",
			"labels": map[interface{}]interface{}{
				"role": "myrole",
			},
		},
		"spec": map[interface{}]interface{}{
			"containers": []interface{}{
				map[interface{}]interface{}{
					"name":  "web",
					"image": "nginx",
					"ports": []interface{}{
						map[interface{}]interface{}{
							"name":          "web",
							"containerPort": "80",
							"protocol":      "TCP",
						},
					},
				},
			},
		},
	}
	got := GetType(testYml)
	if fmt.Sprintf("%v", got) != want {
		log.Fatal().Msgf("parsed different, got: %v, want: %v", got, want)
	}
}
