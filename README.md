# d8ff
! WIP Not yet usable
This project can someday diff two k8s yamls with mutliple objects.

## usage


## Concept
The command line program `d8ff`  takes two yaml files as input parameters and compares them regardless of object order. Each object (split through `---`) will be identified and compared separately. 

### d8ff steps
* read two input files from command line flags
* read data in some kind of struct 
* split the file into all objects
* identify the objects by kind and name
* sort all lines without impacting yaml integrity
* make diff and print output