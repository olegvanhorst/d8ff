package main

import "fmt"

func GetType(value interface{}) interface{} {
	switch v := value.(type) {
	case []interface{}:
		return cleanupInterfaceArray(v)
	case map[interface{}]interface{}:
		return cleanupInterfaceMap(v)
	case string:
		return v
	default:
		return nil
	}
}

func cleanupInterfaceMap(v map[interface{}]interface{}) interface{} {
	res := make(map[string]interface{})
	for k, v := range v {
		res[fmt.Sprintf("%v", k)] = GetType(v)
	}
	return res
}

func cleanupInterfaceArray(v []interface{}) []interface{} {
	res := make([]interface{}, len(v))
	for i, v := range v {
		res[i] = GetType(v)
	}
	return res
}
