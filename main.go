package main

import (
	"io/ioutil"
	"reflect"

	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
)

// func init() {
//	if len(os.Args) != 3 {
//		log.Error().Msgf("Usage: ./d8ff <file1>")
//	}
//}

func main() {

	file1 := "testYml/pod1.yaml" // os.Args[1]
	file2 := "testYml/pod2.yaml" // os.Args[1]

	m1, err := readFile(file1)
	if err != nil {
		log.Error().Msgf("could not read file: %v", err)
	}
	m2, err := readFile(file2)
	if err != nil {
		log.Error().Msgf("could not read file: %v", err)
	}

	pm1 := GetType(m1)
	pm2 := GetType(m2)
	log.Info().Msgf("pm1: %v", pm1)
	log.Info().Msgf("pm2: %v", pm2)

	nub := reflect.DeepEqual(pm1, pm2)
	log.Info().Msgf("equal? %v", nub)
}

// TODO like this: https://github.com/go-yaml/yaml/issues/139#issuecomment-220072190

// readFile opens a file and unmarshal the data
func readFile(fileName string) (m map[interface{}]interface{}, err error) {
	log.Debug().Msgf("read file: %s", fileName)
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(file, &m)
	if err != nil {
		return nil, err
	}
	return m, nil
}
